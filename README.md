# plane-puzzle (lights out)

* [game pattern](https://ampcpmgp.gitlab.io/plane-puzzle/pattern.html)
* [3x3 example](https://ampcpmgp.gitlab.io/plane-puzzle/mock.html?__amMocktimes__=%255B%255B%2522setSideNum%2522%252C3%255D%255D)
* [5x5 + 3 color](https://ampcpmgp.gitlab.io/plane-puzzle/mock.html?__amMocktimes__=%255B%255B%2522setInitialColors%2522%252C3%255D%255D)


## Setup

```
npm i
npm start
```

access //localhost:1234
