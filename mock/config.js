import mock from 'am-mocktimes'
import * as actions from '../src/actions'
import state from '../src/state'
import sleep from '../src/utils/sleep'

const {
  sideNum
} = state

mock(Object.assign({
  sleep,
  async breakableClick (num) {
    for (let variable of Array.from({length: num})) {
      const randomX = actions.getRandomSideNum()
      const randomY = actions.getRandomSideNum()
      await sleep(5)
      actions.advanceColorValuesAround(randomX, randomY)
    }
  },
  async easySettings () {
    await sleep(0)
    actions.setEasySettings()
  }
}, actions))
