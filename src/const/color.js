export default {
  CUBE: {
    DEFAULT: '#999',
    MOUSE_OVER: '#bbddff'
  },
  WHITE: '#fff',
  BLUE: '#0000ff',
  GREEN: '#00ff00',
  YELLOW: '#ffff00',
  PINK: '#ff00ff',
  RED: '#ff0000'
}
