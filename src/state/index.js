import { observable } from 'dob'

const state = observable({
  mouseCoords: {
    x: 0,
    y: 0
  },
  sideNum: 0,
  colors: [],
  cube: []
})

export default state
