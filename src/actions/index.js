import { Action } from 'dob'
import state from '../state'
import Color from '../const/color'

const {
  cube,
  mouseCoords
} = state

export const setMouseCoords = (x, y) => {
  Action(() => {
    mouseCoords.x = x
    mouseCoords.y = y
  })
}

export const setRandomColor = (cubeItem) => {
  const randomColor = state.colors[parseInt(Math.random() * (state.colors.length - 1), 10) + 1]
  cubeItem.color = randomColor
}

export const getRandomSideNum = () => parseInt(Math.random() * state.sideNum, 10)

export const setRandomColors = () => {
  const num = parseInt(Math.random() * 7, 10) + 3
  Array.from({length: num}).forEach(() => {
    const randomX = getRandomSideNum()
    const randomY = getRandomSideNum()

    const cubeItem = state.cube[randomX][randomY]
    setRandomColor(cubeItem)
  })
}

export const setInitialData = () => {
  Action(() => {
    setCube(state.cube)
    setRandomColors()
  })
}

export const setSideNum = (num) => {
  state.sideNum = num
}

const getCubeData = () => {
  return Object.assign({}, {
    color: Color.WHITE
  })
}

export const setCube = (cube, depth = 1) => {
  cube.length = state.sideNum
  Array.from(cube).forEach((_, i) => {
    if (depth === 2) {
      cube[i] = getCubeData()
      return
    }
    setCube(cube[i] = [], depth + 1)
  })
}

export const setColors = (colors) => {
  state.colors = colors
}

export const setInitialColors = (num) => {
  switch (num) {
    case 3:
      setColors([Color.WHITE, Color.BLUE, Color.GREEN])
      break
    case 4:
      setColors([Color.WHITE, Color.BLUE, Color.GREEN, Color.YELLOW])
      break
    case 5:
      setColors([Color.WHITE, Color.BLUE, Color.GREEN, Color.YELLOW, Color.PINK])
      break
    default:
      setColors([Color.WHITE, Color.BLUE])
  }
}

export const getCube = (x, y) => {
  if (!state.cube[x]) return
  if (!state.cube[x][y]) return
  return state.cube[x][y]
}

export const getNextColor = color => {
  const index = state.colors.indexOf(color)
  return state.colors[index + 1] || state.colors[0]
}

export const advanceColor = (x, y) => {
  const cube = getCube(x, y)
  if (!cube) return

  cube.color = getNextColor(cube.color)
}

export const setCallbackAllCube = (callback) => {
  Action(() => {
    for (let x = 0; x < state.cube.length; x++) {
      const cubeX = state.cube[x]
      for (let y = 0; y < cubeX.length; y++) {
        const cubeItem = cubeX[y]
        callback(cubeItem)
      }
    }
  })
}

export const setEasySettings = () => {
  Action(() => {
    setCallbackAllCube(cubeItem => cubeItem.color = state.colors[0])

    cube[3][3].color = state.colors[1]
    cube[3][2].color = state.colors[1]
    cube[3][4].color = state.colors[1]
    cube[2][3].color = state.colors[1]
    cube[4][3].color = state.colors[1]
  })
}

export const advanceColorValuesAround = (x, y) => {
  advanceColor(x, y)
  advanceColor(x - 1, y)
  advanceColor(x + 1, y)
  advanceColor(x, y - 1)
  advanceColor(x, y + 1)
}

export const checkClear = () => {
  let isClear = true
  const firstColor = cube[0][0].color
  setCallbackAllCube(cubeItem => {
    if (cubeItem.color !== firstColor) {
      isClear = false
    }
  })

  if (isClear) setTimeout(() => window.alert('Cleared!'), 10)
}
